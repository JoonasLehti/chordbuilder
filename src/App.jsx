import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import ChordBuilder from './ChordBuilder'

function App() {
  const [count, setCount] = useState(0)

  return (
    <>
      <ChordBuilder></ChordBuilder>
    </>
  )
}

export default App
