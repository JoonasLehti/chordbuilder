import { useEffect, useState } from "react";

const Interface = () => {
  const [FirstString, setFirstString] = useState("E");
  const [SecondString, setSecondString] = useState("A");
  const [ThirdString, setThirdString] = useState("D");
  const [FourthString, setFourthString] = useState("G");
  const [FifthString, setFifthString] = useState("B");
  const [SixthString, setSixthString] = useState("E");
  const [chord, setChord] = useState();

  function getFirstString(event) {
    setFirstString(event.target.value);
    console.log(FirstString);
    changeInputValue();
  }

  useEffect(() => {
    console.log(FirstString);
  }, [FirstString])

  useEffect(() => {
    console.log(SecondString);
  }, [SecondString])

  useEffect(() => {
    console.log(ThirdString);
  }, [ThirdString])

  useEffect(() => {
    console.log(FourthString);
  }, [FourthString])

  useEffect(() => {
    console.log(FifthString);
  }, [FifthString])

  useEffect(() => {
    console.log(SixthString);
  }, [SixthString])

  useEffect(() => {
    console.log(chord)
  }, [chord])

  function getSecondString(event) {
    setSecondString(event.target.value);
    changeInputValue();
  }

  function getThirdString(event) {
    setThirdString(event.target.value);
    console.log(ThirdString);
    changeInputValue();
  }

  function getFourthString(event) {
    setFourthString(event.target.value);
    console.log(FourthString);
    changeInputValue();
  }

  function getFifthString(event) {
    setFifthString(event.target.value);
    console.log(FifthString);
    changeInputValue();
  }

  function getSixthString(event) {
    setSixthString(event.target.value);
    console.log(SixthString);
    changeInputValue();
  }

  function changeInputValue() {
    setChord(FirstString + SecondString + ThirdString + FourthString + FifthString + SixthString);
  }

  return (
    <>
      <h3><input type="text" disabled /></h3>
      <div className="chordChart">
        <div className="firstString">
          <input type="radio" id="E" name="firstString" value="E" onChange={getFirstString} checked></input>
          <label htmlFor="E">E</label><br></br>
          <input type="radio" id="F" name="firstString" value="F" onChange={getFirstString}></input>
          <label htmlFor="F">F</label><br></br>
          <input type="radio" id="F#" name="firstString" value="F#" onChange={getFirstString}></input>
          <label htmlFor="F#">F#</label><br></br>
          <input type="radio" id="G" name="firstString" value="G" onChange={getFirstString}></input>
          <label htmlFor="G">G</label><br></br>
          <input type="radio" id="G#" name="firstString" value="G#" onChange={getFirstString}></input>
          <label htmlFor="G#">G#</label><br></br>
        </div>
        <div className="SecondString">
          <input type="radio" id="A" name="SecondString" value="A" onChange={getSecondString} checked></input>
          <label htmlFor="A">A</label><br></br>
          <input type="radio" id="Bb" name="SecondString" value="Bb" onChange={getSecondString}></input>
          <label htmlFor="Bb">Bb</label><br></br>
          <input type="radio" id="B" name="SecondString" value="B" onChange={getSecondString}></input>
          <label htmlFor="B">B</label><br></br>
          <input type="radio" id="C" name="SecondString" value="C" onChange={getSecondString}></input>
          <label htmlFor="C">C</label><br></br>
          <input type="radio" id="C#" name="SecondString" value="C#" onChange={getSecondString}></input>
          <label htmlFor="C#">C#</label><br></br>
        </div>
        <div className="thirdString">
          <input type="radio" id="D" name="thirdString" value="D" onChange={getThirdString} checked></input>
          <label htmlFor="D">D</label><br></br>
          <input type="radio" id="Eb" name="thirdString" value="Eb" onChange={getThirdString}></input>
          <label htmlFor="Eb">Eb</label><br></br>
          <input type="radio" id="E" name="thirdString" value="E" onChange={getThirdString}></input>
          <label htmlFor="E">E</label><br></br>
          <input type="radio" id="F" name="thirdString" value="F" onChange={getThirdString}></input>
          <label htmlFor="F">F</label><br></br>
          <input type="radio" id="F#" name="thirdString" value="F#" onChange={getThirdString}></input>
          <label htmlFor="F#">F#</label><br></br>
        </div>
        <div className="fourthString">
          <input type="radio" id="G" name="fourthString" value="G" onChange={getFourthString} checked></input>
          <label htmlFor="G">G</label><br></br>
          <input type="radio" id="G#" name="fourthString" value="G#" onChange={getFourthString}></input>
          <label htmlFor="G#">G#</label><br></br>
          <input type="radio" id="A" name="fourthString" value="A" onChange={getFourthString}></input>
          <label htmlFor="A">A</label><br></br>
          <input type="radio" id="Bb" name="fourthString" value="Bb" onChange={getFourthString}></input>
          <label htmlFor="Bb">Bb</label><br></br>
          <input type="radio" id="B" name="fourthString" value="B" onChange={getFourthString}></input>
          <label htmlFor="B">B</label><br></br>
        </div>
        <div className="fifthString">
          <input type="radio" id="B" name="fifthString" value="B" onChange={getFifthString} checked></input>
          <label htmlFor="B">B</label><br></br>
          <input type="radio" id="C" name="fifthString" value="C" onChange={getFifthString}></input>
          <label htmlFor="C">C</label><br></br>
          <input type="radio" id="C#" name="fifthString" value="C#" onChange={getFifthString}></input>
          <label htmlFor="C#">C#</label><br></br>
          <input type="radio" id="D" name="fifthString" value="D" onChange={getFifthString}></input>
          <label htmlFor="D">D</label><br></br>
          <input type="radio" id="Eb" name="fifthString" value="Eb" onChange={getFifthString}></input>
          <label htmlFor="Eb">Eb</label><br></br>
        </div>
        <div className="sixthString">
          <input type="radio" id="E" name="sixthString" value="E" onChange={getSixthString} checked></input>
          <label htmlFor="E">E</label><br></br>
          <input type="radio" id="F" name="sixthString" value="F" onChange={getSixthString}></input>
          <label htmlFor="F">F</label><br></br>
          <input type="radio" id="F#" name="sixthString" value="F#" onChange={getSixthString}></input>
          <label htmlFor="F#">F#</label><br></br>
          <input type="radio" id="G" name="sixthString" value="G" onChange={getSixthString}></input>
          <label htmlFor="G">G</label><br></br>
          <input type="radio" id="G#" name="sixthString" value="G#" onChange={getSixthString}></input>
          <label htmlFor="G#">G#</label><br></br>
        </div>
      </div>
    </>
  )
}

export default Interface;